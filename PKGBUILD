# Maintainer: Márcio Sousa Rocha <marciosr10@gmail.com>

pkgname=leao
java=8
_pkgver=1.0
year=2020
pkgver=${year}.${_pkgver}
pkgrel=2.1
arch=(any)
pkgdesc='Programa para a tributação do Imposto sobre a Renda das Pessoas Físicas.'
url='http://www.receita.fazenda.gov.br'
license=('custom')
makedepends=('unzip')
install=${pkgname}.install

DLAGENTS=('https::/usr/bin/curl -k -o %o %u')

source=("https://downloadirpf.receita.fazenda.gov.br/irpf/${year}/${pkgname}/LEAO${year}v${_pkgver}.zip"
        "https://metainfo.manjariando.com.br/${pkgname}/gov.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        "$pkgname")
sha256sums=('ffccc5e8f2999e8314e35c217307fa5dfe0472fa82fc0667d5bb221c90d9f0e1'
            'fb7474d783ad321c2abccf513c4dbd948490d063c800035d338d76bbb2008025'
            '9f297fe6b5ae9e4d7ead3dcb2d342eb6e7e0dddf3ed93df0865d8165c13e4a40'
            '559690861cf9b4123cb2a171123e23111c65d7a439d6ef5197c007f484f54d13'
            '145bf58e588f58f37e27ad99987e65997b0a596a8a9426de5c2c2443d835fadb'
            'e66edcbe14cc0b5df7cb5f97694ce1eabfe873bbe55d9478ad6465662bcf5e51')

noextract=(LEAO${year}v${_pkgver}.zip)

prepare() {
  unzip LEAO${year}v${_pkgver}.zip
}

_leao_desktop="[Desktop Entry]
Name=Carnê Leão ${year}
Comment=Pagamento mensal de imposto de renda de pessoas físicas
Exec=leao
StartupNotify=true
Icon=leao
Terminal=false
Type=Application
Categories=Network;"

build() {
    cd "${srcdir}"
    echo -e "$_leao_desktop" | tee gov.${pkgname}.desktop
}

package() {
    depends=("java-environment=${java}" 'hicolor-icon-theme' 'desktop-file-utils')
    optdepends=('receitanet: Para envio do IRPF')

    cd "${srcdir}"/LEAO${year}
    rm -f LEAO${year}.exe
    rm -f exec.sh
    rm -f Leia_me.htm
    rm -f exec.bat
    rm -f RFB.ico

    mkdir -p "${pkgdir}"/{usr/bin,usr/share/leao}

    cp -rf lib "${pkgdir}"/usr/share/${pkgname}/
    cp -rf help "${pkgdir}"/usr/share/${pkgname}/

    install -Dm644 PgdCarneLeao.jar "${pkgdir}"/usr/share/${pkgname}/
    install -Dm755 "${srcdir}"/${pkgname} "${pkgdir}"/usr/bin/
    install -Dm644 offline.png "${pkgdir}"/usr/share/${pkgname}/
    install -Dm644 online.png "${pkgdir}"/usr/share/${pkgname}/
    install -Dm644 pgd-updater.jar "${pkgdir}"/usr/share/${pkgname}/
       
    # Appstream
    install -Dm644 "${srcdir}/gov.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/gov.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/gov.${pkgname}.desktop" "${pkgdir}/usr/share/applications/gov.${pkgname}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:JAVA=.*:JAVA=${java}:" "${startdir}/${pkgname}.install"
}
